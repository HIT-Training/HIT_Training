#ifndef __FRANKS_REICH_HIT_VIEWS_LOADING_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_LOADING_SCREEN__

#include <Elementary.h>

namespace fr { namespace views { namespace loading_screen {

struct Screen {
	Evas_Object *mainBox;
	Evas_Object *progressBar;
};

class LoadingScreenFactory {
public:
	void createAndNavigateTo(Evas_Object *parent);

private:
	void _createGui(Evas_Object *parent, Screen &screen);
	void _createMainBox(Evas_Object *parent, Screen &screen);
	void _createProgressBar(Evas_Object *parent, Screen &screen);
};

} // end of namespace loading_screen
} // end of namespace views
} // end of namespace fr

#endif
