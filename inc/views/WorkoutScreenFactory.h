#ifndef __FRANKS_REICH_HIT_VIEWS_WORKOUT_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_WORKOUT_SCREEN__

#include "services/WorkoutService.h"

#include <string>
#include <vector>

#include <Elementary.h>

namespace fr { namespace views { namespace workout_screen {

struct Screen {
	Evas_Object *mainBox;
	Evas_Object *dateLabel;
	Evas_Object *workoutTemplatesList;
};

class WorkoutScreenFactory {
public:
	WorkoutScreenFactory(Evas_Smart_Cb callback);

	void createAndNavigateTo(
		Evas_Object *navigationFrame,
		const fr::services::workout_service::WorkoutTemplates &workoutTemplates);

private:
	Evas_Smart_Cb _callback;

	void _createGui(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::WorkoutTemplates &workoutTemplates);
	void _createMainBox(Evas_Object *parent, Screen &screen);
	void _createDateLabel(Screen &screen);
	void _createWorkoutTemplatesList(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::WorkoutTemplates &workoutTemplates);
	std::string _currentDateString();
};

} // end of namespace workout_screen
} // end of namespace views
} // end of namespace fr

#endif
