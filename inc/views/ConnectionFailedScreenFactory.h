#ifndef __FRANKS_REICH_HIT_VIEWS_CONNECTION_FAILED_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_CONNECTION_FAILED_SCREEN__

#include <Elementary.h>

namespace fr { namespace views { namespace connection_failed_screen {

struct Screen {
	Evas_Object *mainBox;
	Evas_Object *errorTextLabel;
	Evas_Object *confirmButton;
};

class ConnectionFailedScreenFactory {
public:
	ConnectionFailedScreenFactory(Evas_Smart_Cb confirmCallback);
	void createAndNavigateTo(Evas_Object *navigationFrame);

private:
	Evas_Smart_Cb _confirmCallback;

	void _createGui(Evas_Object *parent, Screen &screen);
	void _createMainBox(Evas_Object *parent, Screen &screen);
	void _createErrorTextLabel(Evas_Object *parent, Screen &screen);
	void _createConfirmButton(Evas_Object *parent, Screen &screen);
};

} // end of namespace connection_failed_screen
} // end of namespace views
} // end of namespace fr

#endif
