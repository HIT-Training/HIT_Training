#ifndef __FRANKS_REICH_HIT_VIEWS_CONFIRM_EXERCISE_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_CONFIRM_EXERCISE_SCREEN__

#include "services/ExerciseService.h"
#include <Elementary.h>

namespace fr { namespace views { namespace confirm_exercise_screen {

struct Screen {
	Evas_Object *mainBox;
	Evas_Object *buttonBox;
	Evas_Object *confirmButton;
	Evas_Object *cancelButton;
};

class ConfirmExerciseScreenFactory {
public:
	ConfirmExerciseScreenFactory(
		Evas_Smart_Cb confirmCallback, Evas_Smart_Cb cancelCallback);


	void createAndNavigateTo(
		Evas_Object *navigationFrame,
		fr::services::exercise_service::CompletedExercise &exercise);

private:
	Evas_Smart_Cb _confirmCallback;
	Evas_Smart_Cb _cancelCallback;

	void _createGui(
		Evas_Object *parent,
		fr::services::exercise_service::CompletedExercise &exercise,
		Screen &screen);
	void _createMainBox(Evas_Object *parent, Screen &screen);
	void _createButtonBox(Evas_Object *parent, Screen &screen);
	void _createConfirmButton(Evas_Object *parent, Screen &screen);
	void _createCancelButton(Evas_Object *parent, Screen &screen);
};

} // end of namespace confirm_exercise_screen
} // end of namespace views
} // end of namespace fr

#endif
