#ifndef __FRANKS_REICH_HIT_VIEWS_FINISHED_WORKOUT_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_FINISHED_WORKOUT_SCREEN__

#include <services/Model.h>

#include <Elementary.h>

namespace fr { namespace views { namespace finished_workout {

struct Screen {
	Evas_Object *scroller;
	Evas_Object *mainBox;
	Evas_Object *successIcon;
	Evas_Object *finalTextLabel;
	Evas_Object *confirmButton;
};

class FinishedWorkoutScreenFactory {
public:
	FinishedWorkoutScreenFactory(Evas_Smart_Cb callback);

	void createAndNavigateTo(
		Evas_Object *navigationFrame,
		const fr::services::workout_service::CompletedExercises &completedExercises);

private:
	Evas_Smart_Cb _callback;

	void _createGui(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::CompletedExercises &completedExercises);
	void _createScroller(Evas_Object *parent, Screen &screen);
	void _createMainBox(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::CompletedExercises &completedExercises);
	void _createExerciseSummaries(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::CompletedExercises &completedExercises);
	void _createExerciseSummary(
		Screen &screen,
		const fr::services::exercise_service::CompletedExercise &completedExercise);
	void _createSummaryTitleLabel(Screen &screen, const std::string &title);
	void _createConfirmButton(
		Evas_Object *parent, Screen &screen,
		const fr::services::workout_service::CompletedExercises &completedExercises);
};

} // end of namespace finished_workout
} // end of namespace views
} // end of namespace fr

#endif
