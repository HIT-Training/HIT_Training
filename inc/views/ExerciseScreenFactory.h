#ifndef __FRANKS_REICH_HIT_VIEWS_EXERCISE_SCREEN__
#define __FRANKS_REICH_HIT_VIEWS_EXERCISE_SCREEN__

#include "services/ExerciseService.h"
#include <Elementary.h>

namespace fr { namespace views { namespace exercise_screen {

struct Screen {
	Evas_Object *mainBox;
	Evas_Object *exerciseNameLabel;
	Evas_Object *repetitionsList;
};

class ExerciseScreenFactory {
public:
	ExerciseScreenFactory(Evas_Smart_Cb callback);

	void createAndNavigateTo(
		Evas_Object *navigationFrame,
		const fr::services::exercise_service::Exercise &exercise);

private:
	Evas_Smart_Cb _callback;

	void _createGui(
		Evas_Object *parent,
		const fr::services::exercise_service::Exercise &exercise,
		Screen &screen);
	void _createMainBox(Evas_Object *parent, Screen &screen);
	void _createExerciseNameLabel(
		const fr::services::exercise_service::Exercise &exercise,
		Screen &screen);
	void _createRepetitionList(
		const fr::services::exercise_service::Exercise &exercise,
		Evas_Object *parent, Screen &screen);
};

} // end of namespace exercise_screen
} // end of namespace views
} // end of namespace fr

#endif
