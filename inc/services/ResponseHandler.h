#ifndef __FRANKS_REICH_HIT_SERVICES_RESPONSE_HANDLER__
#define __FRANKS_REICH_HIT_SERVICES_RESPONSE_HANDLER__

#include "services/Model.h"

#include "rapidjson/document.h"

namespace fr { namespace services { namespace response_handler {

typedef void (*GetWorkoutTemplatesServiceCallback)(
	fr::services::workout_service::WorkoutTemplates &);

typedef void (*StartWorkoutServiceCallback)(int32_t);

typedef void (*StartExerciseServiceCallback)(
	fr::services::exercise_service::Exercise &);

typedef void (*FinishExerciseServiceCallback)(bool);

typedef void (*GetWorkoutSummaryServiceCallback)(
	fr::services::workout_service::CompletedExercises &);

class ResponseHandler {
public:
	ResponseHandler(
		GetWorkoutTemplatesServiceCallback getWorkoutTemplatesCallback,
		StartWorkoutServiceCallback startWorkoutServiceCallback,
		StartExerciseServiceCallback startExerciseServiceCallback,
		FinishExerciseServiceCallback finishExerciseServiceCallback,
		GetWorkoutSummaryServiceCallback getWorkoutSummaryServiceCallback);

	void process(std::string message);

private:
	void _processGetWorkoutTemplatesOperation(rapidjson::Document &document);
	void _processStartWorkoutOperation(rapidjson::Document &document);
	void _processStartExerciseOperation(rapidjson::Document &document);
	void _processFinishExerciseOperation(rapidjson::Document &document);
	void _processGetWorkoutSummaryOperation(rapidjson::Document &document);

	GetWorkoutTemplatesServiceCallback _getWorkoutTemplatesServiceCallback;
	StartWorkoutServiceCallback _startWorkoutServiceCallback;
	StartExerciseServiceCallback _startExerciseServiceCallback;
	FinishExerciseServiceCallback _finishExerciseServiceCallback;
	GetWorkoutSummaryServiceCallback _getWorkoutSummaryServiceCallback;
};

} // end of namespace accessory_agent
} // end of namespace services
} // end of namespace fr

#endif
