#ifndef __FRANKS_REICH_HIT_SERVICES_ACCESSORY_AGENT__
#define __FRANKS_REICH_HIT_SERVICES_ACCESSORY_AGENT__

#include "services/ResponseHandler.h"

#include <string>

#include <sap.h>

namespace fr { namespace services { namespace accessory_agent {

void service_connection_terminated_callback(
	sap_peer_agent_h peerAgent,
	sap_socket_h socket,
	sap_service_connection_terminated_reason_e result,
	void *userData);

void data_received_callback(
	sap_socket_h socket,
	unsigned short int channelId,
	unsigned int payloadLength,
	void *buffer,
	void *userData);

void service_connection_created_callback(
	sap_peer_agent_h peerAgent,
	sap_socket_h socket,
	sap_service_connection_result_e result,
	void *userData);

void agent_initialized_callback(
	sap_agent_h agent,
	sap_agent_initialized_result_e result,
	void *userData);

void device_status_changed_callback(
	sap_device_status_e status,
	sap_transport_type_e transportType,
	void *userData);

void peer_agent_updated_callback(
	sap_peer_agent_h peer_agent,
	sap_peer_agent_status_e peer_status,
	sap_peer_agent_found_result_e result,
	void *user_data);

typedef void (*ConnectionEstablishedCallback)();
typedef void (*ConnectionFailedCallback)();

class AccessoryAgent {
public:
	AccessoryAgent(
		ConnectionEstablishedCallback connectionEstablishedCallback,
		ConnectionFailedCallback connectionFailedCallback,
		fr::services::response_handler::GetWorkoutTemplatesServiceCallback getWorkoutTemplatesCallback,
		fr::services::response_handler::StartWorkoutServiceCallback startWorkoutServiceCallback,
		fr::services::response_handler::StartExerciseServiceCallback startExerciseServiceCallback,
		fr::services::response_handler::FinishExerciseServiceCallback finishExerciseServiceCallback,
		fr::services::response_handler::GetWorkoutSummaryServiceCallback getWorkoutSummaryServiceCallback);

	void initialize();
	void destroyPeerAgent();
	void destroySocket();
	void destroyAgent();
	void completeAgentInitialization(sap_agent_h agent);
	void findPeerAgent();
	void setPeerAgent(sap_peer_agent_h peerAgent);
	void setSocket(sap_socket_h socket);
	void createServiceConnection();
	void deinitialize();
	bool send(std::string &message);
	void connectionEstablished();

	void handleMessage(std::string &message);

private:
	sap_agent_h _agent;
	sap_socket_h _socket;
	sap_peer_agent_h _peerAgent;
	bool _agentCreated;
	ConnectionEstablishedCallback _connectionEstablishedCallback;
	ConnectionFailedCallback _connectionFailedCallback;

	bool _initializeAgent();
	bool _initializeAccessoryService();
	fr::services::response_handler::ResponseHandler _responseHandler;
};

} // end of namespace accessory_agent
} // end of namespace services
} // end of namespace fr

#endif

