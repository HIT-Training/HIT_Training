#ifndef __FRANKS_REICH_HIT_SERVICES_WORKOUT_SERVICE__
#define __FRANKS_REICH_HIT_SERVICES_WORKOUT_SERVICE__

#include <string>
#include <vector>
#include <memory>

#include "AccessoryAgent.h"
#include "Model.h"

namespace fr { namespace services { namespace workout_service {

class WorkoutService {
public:
	WorkoutService(
		fr::services::accessory_agent::AccessoryAgent &agent);
	void getWorkoutTemplates();
	void startWorkout(const WorkoutTemplate &workoutTemplate);
	void workoutSummary(int32_t workoutId);

private:
	fr::services::accessory_agent::AccessoryAgent &_agent;

	std::string _getWorkoutTemplatesRequest();
	std::string _startWorkoutRequest(const WorkoutTemplate &workoutTemplate);
	std::string _getWorkoutSummaryRequest(int32_t workoutId);
};

} // end of namespace workout_service
} // end of namespace services
} // end of namespace fr

#endif
