#ifndef __FRANKS_REICH_HIT_SERVICES_MODEL__
#define __FRANKS_REICH_HIT_SERVICES_MODEL__

#include <vector>
#include <string>

namespace fr { namespace services {

namespace exercise_service {

struct Exercise {
	std::string name;
	std::string weight;
	int32_t id;
	std::string previousWeight;
	int32_t previousRepetitions;
	std::string weightUnit;

	Exercise();

	Exercise(
		std::string name, std::string weight, int32_t id,
		std::string previousWeight, int32_t previousRepetitions,
		std::string weightUnit);
};

struct CompletedExercise {
	Exercise exercise;
	int32_t repetitions;

	CompletedExercise();

	CompletedExercise(const Exercise &exercise, int32_t repetitions);

	CompletedExercise(
		const std::string &name, const std::string &weight, int32_t id,
		const std::string previousWeight, int32_t previousRepetitions,
		const std::string weightUnit, int32_t repetitions);
};

} // end of namespace exercise_service

namespace workout_service {

struct WorkoutTemplate {
	std::string name;
	int32_t id;

	WorkoutTemplate(std::string name, int32_t id);

	WorkoutTemplate(const WorkoutTemplate &workoutTemplate);
};

using WorkoutTemplates = std::vector<WorkoutTemplate>;
using CompletedExercises = std::vector<fr::services::exercise_service::CompletedExercise>;

} // end of namespace workout_service

} // end of namespace services
} // end of namespace fr

#endif
