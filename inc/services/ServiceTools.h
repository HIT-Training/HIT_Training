#ifndef __FRANKS_REICH_HIT_SERVICES_SERVICE_TOOLS__
#define __FRANKS_REICH_HIT_SERVICES_SERVICE_TOOLS__

#include <string>

#include "rapidjson/document.h"

namespace fr { namespace services {

std::string documentToString(rapidjson::Document &document);

} // end of namespace services
} // end of namespace fr

#endif
