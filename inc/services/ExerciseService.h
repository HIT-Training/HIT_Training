#ifndef __FRANKS_REICH_HIT_SERVICES_EXERCISE_SERVICE__
#define __FRANKS_REICH_HIT_SERVICES_EXERCISE_SERVICE__

#include <string>
#include <memory>

#include "Model.h"
#include "AccessoryAgent.h"

namespace fr { namespace services { namespace exercise_service {

class ExerciseService {
public:
	ExerciseService(
		fr::services::accessory_agent::AccessoryAgent &agent);
	void startExercise(int32_t workoutId);
	void finishExercise(int32_t workoutId, CompletedExercise &exercise);

private:
	fr::services::accessory_agent::AccessoryAgent &_agent;

	std::string _startExerciseRequest(int32_t workoutId);
	std::string _finishExerciseRequest(int32_t workoutId, CompletedExercise &exercise);
};

} // end of namespace exercise_service
} // end of namespace services
} // end of namespace fr

#endif
