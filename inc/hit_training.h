#ifndef __hit_training_H__
#define __hit_training_H__

#include <services/AccessoryAgent.h>

#include <views/ConfirmExerciseScreenFactory.h>
#include <views/ExerciseScreenFactory.h>
#include <views/WorkoutScreenFactory.h>
#include <views/FinishedWorkoutScreenFactory.h>
#include <views/LoadingScreenFactory.h>
#include <views/ConnectionFailedScreenFactory.h>

#include <app.h>
#include <Elementary.h>
#include <system_settings.h>
#include <efl_extension.h>
#include <dlog.h>

#ifdef  LOG_TAG
#undef  LOG_TAG
#endif
#define LOG_TAG "hit_workout"

#if !defined(PACKAGE)
#define PACKAGE "net.franks-reich.hit"
#endif

struct MainWindow {
	Evas_Object *window;
	Evas_Object *conform;
	Evas_Object *navigationFrame;
};

struct GlobalModel {
	MainWindow mainWindow;
	int32_t workoutId;
	fr::services::exercise_service::CompletedExercise completedExercise;
};

void startWorkoutCallback(void *data, Evas_Object *obj, void *event_info);
void finishExerciseCallback(void *data, Evas_Object *obj, void *event_info);
void confirmExerciseCallback(void *data, Evas_Object *obj, void *event_info);
void cancelExerciseCallback(void *data, Evas_Object *obj, void *event_info);
void confirmSummaryCallback(void *data, Evas_Object *obj, void *event_info);
void confirmConnectionFailedCallback(void *data, Evas_Object *obj, void *event_info);

void connectionEstablishedCallback();
void connectionFailedCallback();

void getWorkoutTemplatesServiceCallback(
	fr::services::workout_service::WorkoutTemplates &workoutTemplates);
void startWorkoutServiceCallback(int32_t workoutId);
void startExerciseServiceCallback(
	fr::services::exercise_service::Exercise &exercise);
void finishExerciseServiceCallback(bool hasNextExercise);
void getWorkoutSummaryServiceCallback(
	fr::services::workout_service::CompletedExercises &completedExercises);

#endif /* __hit_training_H__ */
