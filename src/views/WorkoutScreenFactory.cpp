#include <views/WorkoutScreenFactory.h>

#include <ctime>
#include <sstream>
#include <algorithm>

#include <dlog.h>

using namespace std;
using namespace fr::services::workout_service;

namespace fr { namespace views { namespace workout_screen {

WorkoutScreenFactory::WorkoutScreenFactory(Evas_Smart_Cb callback) {
	_callback = callback;
}

void WorkoutScreenFactory::createAndNavigateTo(
	Evas_Object *navigationFrame,
	const fr::services::workout_service::WorkoutTemplates &workoutTemplates)
{
	Screen screen;
	_createGui(navigationFrame, screen, workoutTemplates);
	elm_naviframe_item_push(
		navigationFrame, "<wrap=word>Select Workout</wrap>",
		nullptr, nullptr, screen.mainBox, nullptr);
}

void WorkoutScreenFactory::_createGui(
	Evas_Object *parent, Screen &screen, const WorkoutTemplates &workoutTemplates) {
	_createMainBox(parent, screen);
	_createDateLabel(screen);
	_createWorkoutTemplatesList(parent, screen, workoutTemplates);
}

void WorkoutScreenFactory::_createMainBox(Evas_Object *parent, Screen &screen) {
	screen.mainBox = elm_box_add(parent);
	evas_object_size_hint_weight_set(
			screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(screen.mainBox);
	elm_object_content_set(parent, screen.mainBox);
}

void WorkoutScreenFactory::_createDateLabel(Screen &screen) {
	screen.dateLabel = elm_label_add(screen.mainBox);
	ostringstream oss;
	oss << "<align=center>" << _currentDateString() << "</align>";
	elm_object_text_set(screen.dateLabel, oss.str().c_str());
	evas_object_size_hint_weight_set(screen.dateLabel, 0.0, 0.0);
	evas_object_size_hint_align_set(
			screen.dateLabel, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_size_hint_min_set(screen.dateLabel, 50, 50);
	evas_object_show(screen.dateLabel);
	elm_box_pack_end(screen.mainBox, screen.dateLabel);
}

string WorkoutScreenFactory::_currentDateString() {
	auto systemTime = std::time(nullptr);
	auto localTime = *std::localtime(&systemTime);
	char buffer[22];
	std::strftime(buffer, sizeof(buffer), "%m/%d/%Y", &localTime);
	return std::string(buffer);
}

char *genlist_text_get(void *data, Evas_Object *obj, const char *part) {
	if (strcmp(part, "elm.text") == 0) {
		auto *workoutTemplate = static_cast<WorkoutTemplate *>(data);
		return strdup(workoutTemplate->name.c_str());
	}

	return nullptr;
}

void genlist_delete_list_data(void *data, Evas_Object *list) {
	WorkoutTemplate *workoutTemplate = static_cast<WorkoutTemplate *>(data);
	delete workoutTemplate;
}

void WorkoutScreenFactory::_createWorkoutTemplatesList(
	Evas_Object *parent, Screen &screen,
	const WorkoutTemplates &workoutTemplates)
{
	Elm_Genlist_Item_Class *itemClass = elm_genlist_item_class_new();
	itemClass->item_style = "1text";
	itemClass->func.text_get = genlist_text_get;
	itemClass->func.del = genlist_delete_list_data;

	screen.workoutTemplatesList = elm_genlist_add(screen.mainBox);
	evas_object_size_hint_weight_set(
			screen.workoutTemplatesList, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(
			screen.workoutTemplatesList, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_scroller_single_direction_set(
		screen.workoutTemplatesList, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
	elm_genlist_homogeneous_set(screen.workoutTemplatesList, EINA_TRUE);
	elm_genlist_mode_set(screen.workoutTemplatesList, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(
		screen.workoutTemplatesList,
		"clicked,double",
		_callback,
		parent);

	for (const auto &workoutTemplate: workoutTemplates) {
		WorkoutTemplate *workoutTemplatePtr = new WorkoutTemplate(workoutTemplate);
		auto *listItem = elm_genlist_item_append(
			screen.workoutTemplatesList, itemClass, workoutTemplatePtr, nullptr,
			ELM_GENLIST_ITEM_NONE, nullptr, nullptr);
		elm_genlist_item_select_mode_set(
			listItem, ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
	}

	elm_genlist_item_class_free(itemClass);
	evas_object_show(screen.workoutTemplatesList);
	elm_box_pack_end(screen.mainBox, screen.workoutTemplatesList);
}

} // end of namespace workout_screen
} // end of namespace views
} // end of namespace fr
