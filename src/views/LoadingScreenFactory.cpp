#include "views/LoadingScreenFactory.h"

namespace fr { namespace views { namespace loading_screen {

void LoadingScreenFactory::createAndNavigateTo(Evas_Object *parent) {
	Screen screen;
	_createGui(parent, screen);
	elm_naviframe_item_push(
		parent, "<wrap=word>HIT Workout</wrap>",
		nullptr, nullptr, screen.mainBox, nullptr);
}

void LoadingScreenFactory::_createGui(Evas_Object *parent, Screen &screen) {
	_createMainBox(parent, screen);
	_createProgressBar(parent, screen);
}

void LoadingScreenFactory::_createMainBox(Evas_Object *parent, Screen &screen) {
	screen.mainBox = elm_box_add(parent);
	evas_object_size_hint_weight_set(
			screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(screen.mainBox);
	elm_object_content_set(parent, screen.mainBox);
}

void LoadingScreenFactory::_createProgressBar(Evas_Object *parent, Screen &screen) {
	screen.progressBar = elm_progressbar_add(screen.mainBox);
	elm_object_style_set(screen.progressBar, "process");
	elm_progressbar_pulse_set(screen.progressBar, EINA_TRUE);
	elm_progressbar_pulse(screen.progressBar, EINA_TRUE);
	evas_object_size_hint_align_set(
		screen.progressBar, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_size_hint_weight_set(
		screen.progressBar, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_box_pack_end(screen.mainBox, screen.progressBar);
	evas_object_show(screen.progressBar);
}

} // end of namespace loading_screen
} // end of namespace views
} // end of namespace fr
