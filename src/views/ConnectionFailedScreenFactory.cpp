#include "views/ConnectionFailedScreenFactory.h"

namespace fr { namespace views { namespace connection_failed_screen {

ConnectionFailedScreenFactory::ConnectionFailedScreenFactory(
	Evas_Smart_Cb confirmCallback):
	_confirmCallback(confirmCallback)
{}

void ConnectionFailedScreenFactory::createAndNavigateTo(
	Evas_Object *navigationFrame)
{
	Screen screen;
	_createGui(navigationFrame, screen);
	elm_naviframe_item_push(
		navigationFrame, "<wrap=word>Error</wrap>",
		nullptr, nullptr, screen.mainBox, nullptr);
}

void ConnectionFailedScreenFactory::_createGui(Evas_Object *parent, Screen &screen) {
	_createMainBox(parent, screen);
	_createErrorTextLabel(parent, screen);
	_createConfirmButton(parent, screen);
}

void ConnectionFailedScreenFactory::_createMainBox(
	Evas_Object *parent, Screen &screen)
{
	screen.mainBox = elm_box_add(parent);
	evas_object_size_hint_weight_set(
			screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(screen.mainBox);
	elm_object_content_set(parent, screen.mainBox);
}

void ConnectionFailedScreenFactory::_createErrorTextLabel(
	Evas_Object *parent, Screen &screen)
{
	screen.errorTextLabel = elm_label_add(screen.mainBox);
	elm_object_text_set(screen.errorTextLabel, "Could not establish service connection");
	evas_object_size_hint_weight_set(screen.errorTextLabel, 0.0, 0.0);
	evas_object_size_hint_align_set(
			screen.errorTextLabel, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_label_wrap_width_set (screen.errorTextLabel, 216);
	evas_object_show(screen.errorTextLabel);
	elm_box_pack_end(screen.mainBox, screen.errorTextLabel);
}

void ConnectionFailedScreenFactory::_createConfirmButton(
	Evas_Object *parent, Screen &screen)
{
	screen.confirmButton = elm_button_add(screen.mainBox);
	evas_object_show(screen.confirmButton);
	evas_object_smart_callback_add(
		screen.confirmButton, "clicked", _confirmCallback, parent);
	elm_object_text_set(screen.confirmButton, "Ok");
	evas_object_size_hint_weight_set(screen.confirmButton, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(
			screen.confirmButton, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(screen.mainBox, screen.confirmButton);
}

} // end of namespace connection_failed_screen
} // end of namespace views
} // end of namespace fr
