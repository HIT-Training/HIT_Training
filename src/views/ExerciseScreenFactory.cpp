#include <views/ExerciseScreenFactory.h>

#include <sstream>

using namespace std;
using namespace fr::services::exercise_service;

namespace fr { namespace views { namespace exercise_screen {

ExerciseScreenFactory::ExerciseScreenFactory(Evas_Smart_Cb callback) {
	_callback = callback;
}

void ExerciseScreenFactory::createAndNavigateTo(
	Evas_Object *navigationFrame, const Exercise &exercise)
{
	Screen screen;
	_createGui(navigationFrame, exercise, screen);
	elm_naviframe_item_push(
		navigationFrame, "<wrap=word>Exercise</wrap>",
		nullptr, nullptr, screen.mainBox, nullptr);
}

void ExerciseScreenFactory::_createGui(
	Evas_Object *parent, const Exercise &exercise, Screen &screen)
{
	_createMainBox(parent, screen);
	_createExerciseNameLabel(exercise, screen);
	_createRepetitionList(exercise, parent, screen);
}

void ExerciseScreenFactory::_createMainBox(Evas_Object *parent, Screen &screen) {
	screen.mainBox = elm_box_add(parent);
	evas_object_size_hint_weight_set(
			screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_show(screen.mainBox);
	elm_object_content_set(parent, screen.mainBox);
}

void ExerciseScreenFactory::_createExerciseNameLabel(
	const Exercise &exercise, Screen &screen)
{
	screen.exerciseNameLabel = elm_label_add(screen.mainBox);
	ostringstream oss;
	oss << "<align=center wrap=word>" << exercise.name.c_str() << "</align>";
	elm_object_text_set(screen.exerciseNameLabel, oss.str().c_str());
	evas_object_size_hint_weight_set(screen.exerciseNameLabel, 0.0, 0.0);
	evas_object_size_hint_align_set(
		screen.exerciseNameLabel, EVAS_HINT_FILL, EVAS_HINT_FILL);
	//evas_object_size_hint_min_set(screen.exerciseNameLabel, 50, 50);
	elm_label_wrap_width_set (screen.exerciseNameLabel, 216);
	evas_object_show(screen.exerciseNameLabel);
	elm_box_pack_end(screen.mainBox, screen.exerciseNameLabel);
}

char *genlist_text_get(void *data, Evas_Object *obj, const char *part) {
	if (strcmp(part, "elm.text") == 0) {
		ostringstream oss;
		auto *completedExercise = static_cast<CompletedExercise *>(data);
		oss << completedExercise->repetitions;
		return strdup(oss.str().c_str());
	}

	return nullptr;
}

void genlist_delete_list_data(void *data, Evas_Object *list) {
	CompletedExercise *completedExercise = static_cast<CompletedExercise *>(data);
	delete completedExercise;
}

void ExerciseScreenFactory::_createRepetitionList(
	const Exercise &exercise, Evas_Object *parent, Screen &screen)
{
	Elm_Genlist_Item_Class *itemClass = elm_genlist_item_class_new();
	itemClass->item_style = "1text";
	itemClass->func.text_get = genlist_text_get;
	itemClass->func.del = genlist_delete_list_data;

	screen.repetitionsList = elm_genlist_add(screen.mainBox);
	evas_object_size_hint_weight_set(
			screen.repetitionsList, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(
			screen.repetitionsList, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_scroller_single_direction_set(
		screen.repetitionsList, ELM_SCROLLER_SINGLE_DIRECTION_HARD);
	elm_genlist_homogeneous_set(screen.repetitionsList, EINA_TRUE);
	elm_genlist_mode_set(screen.repetitionsList, ELM_LIST_COMPRESS);
	evas_object_smart_callback_add(
		screen.repetitionsList,
		"clicked,double",
		_callback,
		parent);

	for (int i = 0; i < 20; i++) {
		ostringstream oss;
		oss << i;
		CompletedExercise *completedExercise = new CompletedExercise(exercise, i);
		auto * listItem = elm_genlist_item_append(
			screen.repetitionsList, itemClass, completedExercise, nullptr,
			ELM_GENLIST_ITEM_NONE, nullptr, nullptr);
		elm_genlist_item_select_mode_set(
			listItem, ELM_OBJECT_SELECT_MODE_DISPLAY_ONLY);
	}

	elm_genlist_item_class_free(itemClass);
	evas_object_show(screen.repetitionsList);
	elm_box_pack_end(screen.mainBox, screen.repetitionsList);
}

} // end of namespace exrecise_screen
} // end of namespace views
} // end of namespace fr
