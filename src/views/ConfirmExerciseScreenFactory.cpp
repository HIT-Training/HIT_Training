#include <views/ConfirmExerciseScreenFactory.h>
#include <sstream>

using namespace std;
using namespace fr::services::exercise_service;

namespace fr { namespace views { namespace confirm_exercise_screen {

ConfirmExerciseScreenFactory::ConfirmExerciseScreenFactory(
	Evas_Smart_Cb confirmCallback,
	Evas_Smart_Cb cancelCallback)
{
	_confirmCallback = confirmCallback;
	_cancelCallback = cancelCallback;
}

void ConfirmExerciseScreenFactory::createAndNavigateTo(
	Evas_Object *navigationFrame, CompletedExercise &exercise)
{
	Screen screen;
	_createGui(navigationFrame, exercise, screen);
	elm_naviframe_item_push(
		navigationFrame, "<wrap=word>Exercise</wrap>",
		nullptr, nullptr, screen.mainBox, nullptr);
}

void ConfirmExerciseScreenFactory::_createGui(
	Evas_Object *parent, CompletedExercise &exercise, Screen &screen)
{
	_createMainBox(parent, screen);
	_createButtonBox(parent, screen);
	_createCancelButton(parent, screen);
	_createConfirmButton(parent, screen);
}

void ConfirmExerciseScreenFactory::_createMainBox(
	Evas_Object *parent, Screen &screen)
{
	screen.mainBox = elm_box_add(parent);
	evas_object_size_hint_weight_set(
			screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(
			screen.mainBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(screen.mainBox);
	elm_object_content_set(parent, screen.mainBox);
}

void ConfirmExerciseScreenFactory::_createButtonBox(
	Evas_Object *parent, Screen &screen)
{
	screen.buttonBox = elm_box_add(screen.mainBox);
	elm_box_horizontal_set(screen.buttonBox, EINA_TRUE);
	evas_object_size_hint_weight_set(
			screen.buttonBox, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(
			screen.buttonBox, EVAS_HINT_FILL, 0.0);
	evas_object_show(screen.buttonBox);
	elm_box_pack_end(screen.mainBox, screen.buttonBox);
}

void ConfirmExerciseScreenFactory::_createConfirmButton(
	Evas_Object *parent, Screen &screen)
{
	screen.confirmButton = elm_button_add(screen.buttonBox);
	evas_object_show(screen.confirmButton);
	evas_object_smart_callback_add(
		screen.confirmButton, "clicked", _confirmCallback, parent);
	elm_object_text_set(screen.confirmButton, "Ok");
	evas_object_size_hint_weight_set(screen.confirmButton, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(
			screen.confirmButton, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(screen.buttonBox, screen.confirmButton);
}

void ConfirmExerciseScreenFactory::_createCancelButton(
	Evas_Object *parent, Screen &screen)
{
	screen.cancelButton = elm_button_add(screen.buttonBox);
	evas_object_show(screen.cancelButton);
	evas_object_smart_callback_add(
			screen.cancelButton, "clicked", _cancelCallback, parent);
	elm_object_text_set(screen.cancelButton, "Cancel");
	evas_object_size_hint_weight_set(screen.cancelButton, EVAS_HINT_EXPAND, 0.0);
	evas_object_size_hint_align_set(
			screen.cancelButton, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(screen.buttonBox, screen.cancelButton);
}

} // end of namespace exrecise_screen
} // end of namespace views
} // end of namespace fr
