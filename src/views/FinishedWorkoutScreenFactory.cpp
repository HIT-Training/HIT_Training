#include "views/FinishedWorkoutScreenFactory.h"

#include <sstream>

using namespace std;
using namespace fr::services::workout_service;
using namespace fr::services::exercise_service;

namespace fr { namespace views { namespace finished_workout {

FinishedWorkoutScreenFactory::FinishedWorkoutScreenFactory(Evas_Smart_Cb callback) {
	_callback = callback;
}

void FinishedWorkoutScreenFactory::createAndNavigateTo(
	Evas_Object *navigationFrame,
	const CompletedExercises &completedExercises)
{
	Screen screen;
	_createGui(navigationFrame, screen, completedExercises);
	elm_naviframe_item_push(
		navigationFrame, "<wrap=word>Summary</wrap>",
		nullptr, nullptr, screen.scroller, nullptr);
}

void FinishedWorkoutScreenFactory::_createGui(
	Evas_Object *parent, Screen &screen,
	const CompletedExercises &completedExercises)
{
	_createScroller(parent, screen);
	_createMainBox(parent, screen, completedExercises);
	_createExerciseSummaries(parent, screen, completedExercises);
	_createConfirmButton(parent, screen, completedExercises);
}

void FinishedWorkoutScreenFactory::_createScroller(
	Evas_Object *parent, Screen &screen)
{
	screen.scroller = elm_scroller_add(parent);
	evas_object_size_hint_weight_set(
		screen.scroller, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(
		screen.scroller, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(screen.scroller);
}

void FinishedWorkoutScreenFactory::_createMainBox(
	Evas_Object *parent, Screen &screen,
	const CompletedExercises &completedExercises)
{
	screen.mainBox = elm_box_add(screen.scroller);
	evas_object_size_hint_weight_set(
		screen.mainBox, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(
		screen.mainBox, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(screen.mainBox);
	elm_object_content_set(screen.scroller, screen.mainBox);
}

void FinishedWorkoutScreenFactory::_createExerciseSummaries(
	Evas_Object *parent, Screen &screen,
	const fr::services::workout_service::CompletedExercises &completedExercises)
{
	for (const auto &completedExercise: completedExercises) {
		_createExerciseSummary(screen, completedExercise);
	}
}

void FinishedWorkoutScreenFactory::_createExerciseSummary(
	Screen &screen,
	const CompletedExercise &completedExercise)
{
	_createSummaryTitleLabel(screen, completedExercise.exercise.name);
}

void FinishedWorkoutScreenFactory::_createSummaryTitleLabel(
	Screen &screen, const std::string &title)
{
	Evas_Object *label = elm_label_add(screen.mainBox);
	ostringstream oss;
	oss << "<wrap=word>" << title << "</wrap>";
	elm_object_text_set(label, oss.str().c_str());
	evas_object_size_hint_align_set(
		label, EVAS_HINT_FILL, 0.0);
	evas_object_show(label);
	elm_box_pack_end(screen.mainBox, label);
}

void FinishedWorkoutScreenFactory::_createConfirmButton(
	Evas_Object *parent, Screen &screen,
	const CompletedExercises &completedExercises)
{
	screen.confirmButton = elm_button_add(screen.mainBox);
	evas_object_show(screen.confirmButton);
	evas_object_smart_callback_add(
		screen.confirmButton, "clicked", _callback, parent);
	elm_object_text_set(screen.confirmButton, "Great!");
	evas_object_size_hint_weight_set(screen.confirmButton, 0.0, 0.0);
	evas_object_size_hint_align_set(
			screen.confirmButton, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(screen.mainBox, screen.confirmButton);
}

} // end of namespace finished_workout
} // end of namespace views
} // end of namespace fr
