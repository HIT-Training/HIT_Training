#include "hit_training.h"

#include "stdio.h"

using namespace std;

using namespace fr::services::workout_service;
using namespace fr::services::exercise_service;
using namespace fr::services::accessory_agent;
using namespace fr::views::workout_screen;
using namespace fr::views::exercise_screen;
using namespace fr::views::confirm_exercise_screen;
using namespace fr::views::finished_workout;
using namespace fr::views::loading_screen;
using namespace fr::views::connection_failed_screen;

auto globalModel = GlobalModel();

AccessoryAgent agent(
	connectionEstablishedCallback,
	connectionFailedCallback,
	getWorkoutTemplatesServiceCallback,
	startWorkoutServiceCallback,
	startExerciseServiceCallback,
	finishExerciseServiceCallback,
	getWorkoutSummaryServiceCallback);

auto workoutService = WorkoutService(agent);
auto exerciseService = ExerciseService(agent);

auto workoutScreen = WorkoutScreenFactory(startWorkoutCallback);
auto exerciseScreen = ExerciseScreenFactory(finishExerciseCallback);
auto confirmExerciseScreen = ConfirmExerciseScreenFactory(
	confirmExerciseCallback, cancelExerciseCallback);
auto finishedWorkoutScreen = FinishedWorkoutScreenFactory(confirmSummaryCallback);
auto loadingScreen = LoadingScreenFactory();
auto connectionFailedScreen = ConnectionFailedScreenFactory(
	confirmConnectionFailedCallback);

void startWorkoutCallback(void *data, Evas_Object *obj, void *event_info) {
	Elm_Object_Item *item = static_cast<Elm_Object_Item *>(event_info);
	WorkoutTemplate *workoutTemplate = static_cast<WorkoutTemplate *>(
		elm_object_item_data_get(item));

	workoutService.startWorkout(*workoutTemplate);
}

void finishExerciseCallback(void *data, Evas_Object *obj, void *event_info) {
	Elm_Object_Item *item = static_cast<Elm_Object_Item *>(event_info);
	CompletedExercise *completedExercise = static_cast<CompletedExercise *>(
		elm_object_item_data_get(item));
	Evas_Object *navigationFrame = static_cast<Evas_Object *>(data);

	globalModel.completedExercise = *completedExercise;
	confirmExerciseScreen.createAndNavigateTo(navigationFrame, *completedExercise);
}

void confirmExerciseCallback(void *data, Evas_Object *obj, void *event_info) {
	exerciseService.finishExercise(
		globalModel.workoutId, globalModel.completedExercise);
}

void cancelExerciseCallback(void *data, Evas_Object *obj, void *event_info) {
	Evas_Object *navigationFrame = static_cast<Evas_Object *>(data);
	elm_naviframe_item_pop(navigationFrame);
	elm_naviframe_item_pop(navigationFrame);
	exerciseService.startExercise(globalModel.workoutId);
}

void confirmSummaryCallback(void *data, Evas_Object *obj, void *event_info) {
	workoutService.getWorkoutTemplates();
}

void confirmConnectionFailedCallback(void *data, Evas_Object *obj, void *event_info) {
	ui_app_exit();
}

void connectionEstablishedCallback() {
	workoutService.getWorkoutTemplates();
}

void connectionFailedCallback() {
	connectionFailedScreen.createAndNavigateTo(
		globalModel.mainWindow.navigationFrame);
}

void getWorkoutTemplatesServiceCallback(
	fr::services::workout_service::WorkoutTemplates &workoutTemplates)
{
	workoutScreen.createAndNavigateTo(
		globalModel.mainWindow.navigationFrame, workoutTemplates);
}

void startWorkoutServiceCallback(int32_t workoutId) {
	globalModel.workoutId = workoutId;
	exerciseService.startExercise(workoutId);
}

void startExerciseServiceCallback(
	fr::services::exercise_service::Exercise &exercise)
{
	exerciseScreen.createAndNavigateTo(
		globalModel.mainWindow.navigationFrame, exercise);
}

void finishExerciseServiceCallback(bool hasNextExercise) {
	if (hasNextExercise) {
		exerciseService.startExercise(globalModel.workoutId);
	} else {
		workoutService.workoutSummary(globalModel.workoutId);
	}
}

void getWorkoutSummaryServiceCallback(CompletedExercises &completedExercises) {
	finishedWorkoutScreen.createAndNavigateTo(
		globalModel.mainWindow.navigationFrame, completedExercises);
}

static void
win_delete_request_cb(void *data, Evas_Object *obj, void *event_info)
{
	ui_app_exit();
}

static void
win_back_cb(void *data, Evas_Object *evasObject, void *event_info)
{
	MainWindow *mainWindowPtr = static_cast<MainWindow *>(data);
	elm_win_lower(mainWindowPtr->window);
}

static void
create_base_gui(MainWindow *mainWindow)
{
	mainWindow->window = elm_win_util_standard_add(PACKAGE, PACKAGE);
	elm_win_autodel_set(mainWindow->window, EINA_TRUE);

	if (elm_win_wm_rotation_supported_get(mainWindow->window)) {
		int rots[4] = { 0, 90, 180, 270 };
		elm_win_wm_rotation_available_rotations_set(
			mainWindow->window, (const int *)(&rots), 4);
	}

	evas_object_smart_callback_add(
			mainWindow->window, "delete,request", win_delete_request_cb, NULL);
	eext_object_event_callback_add(
			mainWindow->window, EEXT_CALLBACK_BACK, win_back_cb, mainWindow);

	mainWindow->conform = elm_conformant_add(mainWindow->window);
	elm_win_indicator_mode_set(mainWindow->window, ELM_WIN_INDICATOR_SHOW);
	elm_win_indicator_opacity_set(mainWindow->window, ELM_WIN_INDICATOR_OPAQUE);
	evas_object_size_hint_weight_set(
			mainWindow->conform, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_win_resize_object_add(
			mainWindow->window, mainWindow->conform);
	evas_object_show(mainWindow->conform);

	mainWindow->navigationFrame = elm_naviframe_add(mainWindow->conform);
	evas_object_show(mainWindow->navigationFrame);
	elm_object_content_set(mainWindow->conform, mainWindow->navigationFrame);

	loadingScreen.createAndNavigateTo(mainWindow->navigationFrame);

	evas_object_show(mainWindow->window);
	globalModel.mainWindow.window = mainWindow->window;
	globalModel.mainWindow.conform = mainWindow->conform;
	globalModel.mainWindow.navigationFrame = mainWindow->navigationFrame;
}

/*
 * Hook to take necessary actions before main event loop starts
 * Initialize UI resources and application's data
 * If this function returns true, the main loop of application starts
 * If this function returns false, the application is terminated
 */
static bool
app_create(void *data)
{
	MainWindow *mainWindow = static_cast<MainWindow *>(data);
	create_base_gui(mainWindow);

	return true;
}

static void
app_control(app_control_h app_control, void *data)
{
	/* Handle the launch request. */
}

static void
app_pause(void *data)
{
}

static void
app_resume(void *data)
{
}

static void
app_terminate(void *data)
{
}

static void
ui_app_lang_changed(app_event_info_h event_info, void *user_data)
{
	char *locale = NULL;
	system_settings_get_value_string(SYSTEM_SETTINGS_KEY_LOCALE_LANGUAGE, &locale);
	elm_language_set(locale);
	free(locale);
	return;
}

static void
ui_app_orient_changed(app_event_info_h event_info, void *user_data)
{
	return;
}

static void
ui_app_region_changed(app_event_info_h event_info, void *user_data)
{
}

static void
ui_app_low_battery(app_event_info_h event_info, void *user_data)
{
}

static void
ui_app_low_memory(app_event_info_h event_info, void *user_data)
{
}

int
main(int argc, char *argv[])
{
	MainWindow mainWindow = {0,};
	int returnCode = 0;

	ui_app_lifecycle_callback_s eventCallback = {0,};
	app_event_handler_h handlers[5] = {NULL, };

	eventCallback.create = app_create;
	eventCallback.terminate = app_terminate;
	eventCallback.pause = app_pause;
	eventCallback.resume = app_resume;
	eventCallback.app_control = app_control;

	ui_app_add_event_handler(
			&handlers[APP_EVENT_LOW_BATTERY],
			APP_EVENT_LOW_BATTERY,
			ui_app_low_battery,
			&mainWindow);

	ui_app_add_event_handler(
			&handlers[APP_EVENT_LOW_MEMORY],
			APP_EVENT_LOW_MEMORY,
			ui_app_low_memory,
			&mainWindow);

	ui_app_add_event_handler(
			&handlers[APP_EVENT_DEVICE_ORIENTATION_CHANGED],
			APP_EVENT_DEVICE_ORIENTATION_CHANGED,
			ui_app_orient_changed,
			&mainWindow);

	ui_app_add_event_handler(
			&handlers[APP_EVENT_LANGUAGE_CHANGED],
			APP_EVENT_LANGUAGE_CHANGED,
			ui_app_lang_changed,
			&mainWindow);

	ui_app_add_event_handler(
			&handlers[APP_EVENT_REGION_FORMAT_CHANGED],
			APP_EVENT_REGION_FORMAT_CHANGED,
			ui_app_region_changed,
			&mainWindow);

	agent.initialize();

	returnCode = ui_app_main(argc, argv, &eventCallback, &mainWindow);
	if (returnCode != APP_ERROR_NONE) {
		dlog_print(DLOG_ERROR, LOG_TAG, "app_main() is failed. err = %d", returnCode);
	}

	return returnCode;
}
