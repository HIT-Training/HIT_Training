#include "services/ResponseHandler.h"

#include "services/WorkoutService.h"

#include <dlog.h>
#include <sstream>

using namespace std;
using namespace rapidjson;
using namespace fr::services::workout_service;
using namespace fr::services::exercise_service;

namespace fr { namespace services { namespace response_handler {

const std::string RESPONSE_HANDLER_LOG_TAG = "Response Handler";

ResponseHandler::ResponseHandler(
		GetWorkoutTemplatesServiceCallback getWorkoutTemplatesCallback,
		StartWorkoutServiceCallback startWorkoutServiceCallback,
		StartExerciseServiceCallback startExerciseServiceCallback,
		FinishExerciseServiceCallback finishExerciseServiceCallback,
		GetWorkoutSummaryServiceCallback getWorkoutSummaryServiceCallback):
	_getWorkoutTemplatesServiceCallback(getWorkoutTemplatesCallback),
	_startWorkoutServiceCallback(startWorkoutServiceCallback),
	_startExerciseServiceCallback(startExerciseServiceCallback),
	_finishExerciseServiceCallback(finishExerciseServiceCallback),
	_getWorkoutSummaryServiceCallback(getWorkoutSummaryServiceCallback)
{}

void ResponseHandler::process(std::string message) {
	ostringstream oss;
	oss << "Determining operation for message: " << message;
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		oss.str().c_str());

	Document document;
	document.Parse(message.c_str());

	auto operation = string(document["operation"].GetString());
	if (operation == "GetWorkoutTemplates") {
		_processGetWorkoutTemplatesOperation(document);
	} else if (operation == "StartWorkout") {
		_processStartWorkoutOperation(document);
	} else if (operation == "StartExercise") {
		_processStartExerciseOperation(document);
	} else if (operation == "FinishExercise") {
		_processFinishExerciseOperation(document);
	} else if (operation == "GetWorkoutSummary") {
		_processGetWorkoutSummaryOperation(document);
	} else {
		dlog_print(
			DLOG_INFO,
			RESPONSE_HANDLER_LOG_TAG.c_str(),
			"Unknown operation");
	}
}

void ResponseHandler::_processGetWorkoutTemplatesOperation(Document &document) {
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		"Processing operation GetWorkoutTemplates");
	auto workoutTemplates = WorkoutTemplates();
	for (auto &workoutTemplateJson: document["workoutTemplates"].GetArray()) {
		auto workoutTemplate = WorkoutTemplate(
			workoutTemplateJson["name"].GetString(),
			workoutTemplateJson["id"].GetInt());
		workoutTemplates.push_back(workoutTemplate);
	}
	_getWorkoutTemplatesServiceCallback(workoutTemplates);
}

void ResponseHandler::_processStartWorkoutOperation(rapidjson::Document &document) {
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		"Processing operation StartWorkout");
	int32_t workoutId = document["workoutId"].GetInt();
	_startWorkoutServiceCallback(workoutId);
}

void ResponseHandler::_processStartExerciseOperation(Document &document) {
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		"Processing operation StartExercise");
	auto exercise = Exercise(
		document["exercise"]["name"].GetString(),
		document["exercise"]["weight"].GetString(),
		document["exercise"]["id"].GetInt(),
		document["exercise"]["previousWeight"].GetString(),
		document["exercise"]["previousRepetitions"].GetInt(),
		document["exercise"]["weightUnit"].GetString());
	_startExerciseServiceCallback(exercise);
}

void ResponseHandler::_processFinishExerciseOperation(rapidjson::Document &document) {
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		"Processing operation FinishExercise");
	bool hasNextExercise = document["hasNextExercise"].GetBool();
	_finishExerciseServiceCallback(hasNextExercise);
}

void ResponseHandler::_processGetWorkoutSummaryOperation(rapidjson::Document &document) {
	dlog_print(
		DLOG_INFO,
		RESPONSE_HANDLER_LOG_TAG.c_str(),
		"Processing operation GetWorkoutSummary");
	auto completedExercises = CompletedExercises();
	for (auto &completedExerciseJson: document["completedExercises"].GetArray()) {
		auto completedExercise = CompletedExercise(
			completedExerciseJson["name"].GetString(),
			completedExerciseJson["weight"].GetString(),
			completedExerciseJson["id"].GetInt(),
			completedExerciseJson["previousWeight"].GetString(),
			completedExerciseJson["previousRepetitions"].GetInt(),
			completedExerciseJson["weightUnit"].GetString(),
			completedExerciseJson["repetitions"].GetInt());
		completedExercises.push_back(completedExercise);
	}
	_getWorkoutSummaryServiceCallback(completedExercises);
}

} // end of namespace accessory_agent
} // end of namespace services
} // end of namespace fr
