#include "services/Model.h"

namespace fr { namespace services {

namespace exercise_service {

Exercise::Exercise() {
	this->id = 0;
	this->name = "";
	this->previousRepetitions = 0;
	this->previousWeight = "";
	this->weight = "";
	this->weightUnit = "";
}

Exercise::Exercise(
	std::string name, std::string weight, int32_t id,
	std::string previousWeight, int32_t previousRepetitions,
	std::string weightUnit)
{
	this->name = name;
	this->id = id;
	this->weight = weight;
	this->previousWeight = previousWeight;
	this->previousRepetitions = previousRepetitions;
	this->weightUnit = weightUnit;
}

CompletedExercise::CompletedExercise():
	exercise(),
	repetitions(0)
{}

CompletedExercise::CompletedExercise(const Exercise &exercise, int32_t repetitions):
	exercise(exercise),
	repetitions(repetitions)
{}

CompletedExercise::CompletedExercise(
	const std::string &name, const std::string &weight, int32_t id,
	const std::string previousWeight, int32_t previousRepetitions,
	const std::string weightUnit, int32_t repetitions):
	exercise(name, weight, id, previousWeight, previousRepetitions, weightUnit),
	repetitions(repetitions)
{}

} // end of namespace exercise_service

namespace workout_service {

WorkoutTemplate::WorkoutTemplate(std::string name, int32_t id) {
	this->name = name;
	this->id = id;
}

WorkoutTemplate::WorkoutTemplate(const WorkoutTemplate &workoutTemplate) {
	this->name = workoutTemplate.name;
	this->id = workoutTemplate.id;
}

using WorkoutTemplates = std::vector<WorkoutTemplate>;
using CompletedExercises = std::vector<fr::services::exercise_service::CompletedExercise>;

} // end of namespace workout_service

} // end of namespace services
} // end of namespace fr
