#include "services/ServiceTools.h"

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

using namespace std;
using namespace rapidjson;

namespace fr { namespace services {

std::string documentToString(rapidjson::Document &document) {
	StringBuffer buffer;
	Writer<StringBuffer> writer(buffer);
	document.Accept(writer);
	return string(buffer.GetString());
}

} // end of namespace services
} // end of namespace fr
