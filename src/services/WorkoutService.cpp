#include "services/WorkoutService.h"

#include "rapidjson/document.h"

#include <dlog.h>

#include <sstream>

#include "services/ServiceTools.h"

using namespace std;
using namespace rapidjson;
using namespace fr::services::exercise_service;
using namespace fr::services::accessory_agent;

namespace fr { namespace services { namespace workout_service {

string WORKOUT_SERVICE_TAG = "WorkoutService";

WorkoutService::WorkoutService(AccessoryAgent &agent):
	_agent(agent)
{}

void WorkoutService::getWorkoutTemplates() {
	dlog_print(
		DLOG_INFO, WORKOUT_SERVICE_TAG.c_str(), "Retrieving workout templates");
	auto message = _getWorkoutTemplatesRequest();
	_agent.send(message);
}

void WorkoutService::startWorkout(const WorkoutTemplate &workoutTemplate) {
	dlog_print(
		DLOG_INFO, WORKOUT_SERVICE_TAG.c_str(), "Starting workout");
	auto message = _startWorkoutRequest(workoutTemplate);
	_agent.send(message);
}

void WorkoutService::workoutSummary(int32_t workoutId) {
	dlog_print(
		DLOG_INFO, WORKOUT_SERVICE_TAG.c_str(), "Retrieving workout summary");
	auto message = _getWorkoutSummaryRequest(workoutId);
	_agent.send(message);
}

string WorkoutService::_getWorkoutTemplatesRequest() {
	Document message;
	message.SetObject();
	message.AddMember("operation", "GetWorkoutTemplates", message.GetAllocator());
	return documentToString(message);
}

string WorkoutService::_startWorkoutRequest(const WorkoutTemplate &workoutTemplate) {
	Document message;
	message.SetObject();
	message.AddMember("operation", "StartWorkout", message.GetAllocator());
	message.AddMember(
		"workoutTemplateId", workoutTemplate.id, message.GetAllocator());
	return documentToString(message);
}

string WorkoutService::_getWorkoutSummaryRequest(int32_t workoutId) {
	Document message;
	message.SetObject();
	message.AddMember("operation", "GetWorkoutSummary", message.GetAllocator());
	message.AddMember("workoutId", workoutId, message.GetAllocator());
	return documentToString(message);
}

} // end of namespace workout_service
} // end of namespace services
} // end of namespace fr
