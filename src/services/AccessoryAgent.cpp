#include "services/AccessoryAgent.h"

#include <sstream>

#include <dlog.h>

using namespace std;
using namespace fr::services::response_handler;

namespace fr { namespace services { namespace accessory_agent {

const std::string PROFILE_ID = "/net/franksreich/hit";
const int CHANNEL_ID = 104;

const std::string ACCESSORY_AGENT_LOG_TAG = "Accessory Agent";

AccessoryAgent::AccessoryAgent(
	ConnectionEstablishedCallback connectionEstablishedCallback,
	ConnectionFailedCallback connectionFailedCallback,
	GetWorkoutTemplatesServiceCallback getWorkoutTemplatesCallback,
	StartWorkoutServiceCallback startWorkoutServiceCallback,
	StartExerciseServiceCallback startExerciseServiceCallback,
	FinishExerciseServiceCallback finishExerciseServiceCallback,
	GetWorkoutSummaryServiceCallback getWorkoutSummaryServiceCallback):
	_agent(nullptr),
	_socket(nullptr),
	_peerAgent(nullptr),
	_agentCreated(false),
	_connectionEstablishedCallback(connectionEstablishedCallback),
	_connectionFailedCallback(connectionFailedCallback),
	_responseHandler(
		getWorkoutTemplatesCallback,
		startWorkoutServiceCallback,
		startExerciseServiceCallback,
		finishExerciseServiceCallback,
		getWorkoutSummaryServiceCallback)
{}

void AccessoryAgent::initialize() {
	if (_initializeAccessoryService()) {
		_initializeAgent();
	}
}

bool AccessoryAgent::_initializeAccessoryService() {
	sap_agent_create(&_agent);
	if (_agent == nullptr) {
		dlog_print(
			DLOG_ERROR,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Could not create Accessory Agent");
		return false;
	} else {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Accessory Agent created");
		return true;
	}
}

bool AccessoryAgent::_initializeAgent() {
	int result = 0;

	do {
		result = sap_agent_initialize(
			_agent, PROFILE_ID.c_str(), SAP_AGENT_ROLE_CONSUMER,
			agent_initialized_callback, this);
		ostringstream oss;
		oss << "initializeAgent, result: " << result;
		dlog_print(DLOG_INFO, ACCESSORY_AGENT_LOG_TAG.c_str(), oss.str().c_str());
	} while (result != SAP_RESULT_SUCCESS);

	return true;
}

void AccessoryAgent::destroyPeerAgent() {
	destroySocket();
	if (_peerAgent != nullptr) {
		sap_peer_agent_destroy(_peerAgent);
		_peerAgent = nullptr;
	}
	_connectionFailedCallback();
}

void AccessoryAgent::destroySocket() {
	if (_socket != nullptr) {
		sap_socket_destroy(_socket);
		_socket = nullptr;
	}
	_connectionFailedCallback();
}

void AccessoryAgent::destroyAgent() {
	if (_agent != nullptr) {
		sap_agent_destroy(_agent);
		_agent = nullptr;
	}
	_connectionFailedCallback();
}

void AccessoryAgent::completeAgentInitialization(sap_agent_h agent) {
	_agent = agent;
	_agentCreated = true;
	sap_set_device_status_changed_cb(
		device_status_changed_callback, this);
}

void AccessoryAgent::findPeerAgent() {
	sap_result_e result = SAP_RESULT_FAILURE;
	result = (sap_result_e) sap_agent_find_peer_agent(
		_agent, peer_agent_updated_callback, this);

	if (result == SAP_RESULT_SUCCESS) {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent call successful");
	} else {
		dlog_print(
			DLOG_ERROR,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent call unsuccessful");
	}
}

void AccessoryAgent::setPeerAgent(sap_peer_agent_h peerAgent) {
	_peerAgent = peerAgent;
}

void AccessoryAgent::setSocket(sap_socket_h socket) {
	_socket = socket;
}

void AccessoryAgent::createServiceConnection() {
	sap_result_e result = SAP_RESULT_FAILURE;
	result = (sap_result_e) sap_agent_request_service_connection(
		_agent, _peerAgent, service_connection_created_callback, this);

	if (result == SAP_RESULT_SUCCESS) {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Service connection requested");
	} else {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Error while requesting service connection");
	}
}

void AccessoryAgent::deinitialize() {
	destroySocket();
	destroyPeerAgent();
	destroyAgent();
}

bool AccessoryAgent::send(string &message) {
	if (_socket != nullptr) {
		sap_socket_send_data(
			_socket,
			CHANNEL_ID,
			message.size(),
			(void *) message.c_str());
		ostringstream oss;
		oss << "Sending message: " << message;
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			oss.str().c_str());
		return true;
	} else {
		dlog_print(
			DLOG_ERROR,
			"ACCESSORY_AGENT_LOG_TAG.c_str()",
			"Socket is null");
		return false;
	}
}

void AccessoryAgent::handleMessage(string &message) {
	_responseHandler.process(message);
}

void AccessoryAgent::connectionEstablished() {
	_connectionEstablishedCallback();
}

void service_connection_terminated_callback(
	sap_peer_agent_h peerAgent,
	sap_socket_h socket,
	sap_service_connection_terminated_reason_e result,
	void *userData)
{
	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	switch (result) {
	case SAP_CONNECTION_TERMINATED_REASON_PEER_DISCONNECTED:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Connection terminated, peer disconnected");
		break;

	case SAP_CONNECTION_TERMINATED_REASON_DEVICE_DETACHED:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Connection terminated, device detached");
		break;

	case SAP_CONNECTION_TERMINATED_REASON_UNKNOWN:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Connection terminated, unknown reason");
		break;
	}

	accessoryAgent->destroySocket();
}

void data_received_callback(
	sap_socket_h socket,
	unsigned short int channelId,
	unsigned int payloadLength,
	void *buffer,
	void *userData)
{
	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	auto message = string((char *) buffer, payloadLength);
	ostringstream oss;
	oss << "Message received: " << message;
	dlog_print(
		DLOG_INFO,
		ACCESSORY_AGENT_LOG_TAG.c_str(),
		oss.str().c_str());

	accessoryAgent->handleMessage(message);
}

void service_connection_created_callback(
	sap_peer_agent_h peerAgent,
	sap_socket_h socket,
	sap_service_connection_result_e result,
	void *userData)
{
	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	switch (result) {
	case SAP_CONNECTION_SUCCESS:
		sap_peer_agent_set_service_connection_terminated_cb(
			peerAgent, service_connection_terminated_callback, accessoryAgent);
		sap_socket_set_data_received_cb(
			socket, data_received_callback, accessoryAgent);
		accessoryAgent->setSocket(socket);
		accessoryAgent->connectionEstablished();
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Service connection established");
		break;

	case SAP_CONNECTION_IN_PROGRESS:
		break;

	case SAP_CONNECTION_ALREADY_EXIST:
		break;

	case SAP_CONNECTION_FAILURE_DEVICE_UNREACHABLE:
		break;

	case SAP_CONNECTION_FAILURE_INVALID_PEERAGENT:
		break;

	case SAP_CONNECTION_FAILURE_NETWORK:
		break;

	case SAP_CONNECTION_FAILURE_PEERAGENT_NO_RESPONSE:
		break;

	case SAP_CONNECTION_FAILURE_PEERAGENT_REJECTED:
		break;

	case SAP_CONNECTION_FAILURE_UNKNOWN:
		break;
	}
}

void agent_initialized_callback(
	sap_agent_h agent,
	sap_agent_initialized_result_e result,
	void *userData)
{
	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	switch (result) {
	case SAP_AGENT_INITIALIZED_RESULT_SUCCESS:
		accessoryAgent->completeAgentInitialization(agent);
		break;
	case SAP_AGENT_INITIALIZED_RESULT_DUPLICATED:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Duplicate agent initialization");
		break;
	case SAP_AGENT_INITIALIZED_RESULT_INVALID_ARGUMENTS:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Agent initialization invalid arguments");
		break;
	case SAP_AGENT_INITIALIZED_RESULT_INTERNAL_ERROR:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Agent initialization internal error");
		break;
	default:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Unknown agent initialization result");
	}
}

void device_status_changed_callback(
	sap_device_status_e status,
	sap_transport_type_e transportType,
	void *userData)
{
	if (transportType == SAP_TRANSPORT_TYPE_BT) {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Status changed for bluetooth connection");
	} else if (transportType == SAP_TRANSPORT_TYPE_TCP) {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Status changed for tcp connection");
	} else if (transportType == SAP_TRANSPORT_TYPE_ALL) {
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Status changed for all connections");
	} else {
		ostringstream oss;
		oss << "Status changed for unknown connection type " << transportType;
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			oss.str().c_str());
		return;
	}

	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	switch (status) {
	case SAP_DEVICE_STATUS_DETACHED:
		accessoryAgent->destroyPeerAgent();
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Device is detached");
		break;
	case SAP_DEVICE_STATUS_ATTACHED:
		accessoryAgent->findPeerAgent();
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Device is attached");
		break;
	default:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Unknown status");
	}
}

void peer_agent_updated_callback(
	sap_peer_agent_h peerAgent,
	sap_peer_agent_status_e peerStatus,
	sap_peer_agent_found_result_e result,
	void *userData)
{
	auto *accessoryAgent = static_cast<AccessoryAgent *>(userData);

	switch (result) {
	case SAP_PEER_AGENT_FOUND_RESULT_DEVICE_NOT_CONNECTED:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent result, device is not connected");
		break;

	case SAP_PEER_AGENT_FOUND_RESULT_FOUND:
		if (peerStatus == SAP_PEER_AGENT_STATUS_AVAILABLE) {
			accessoryAgent->setPeerAgent(peerAgent);
			accessoryAgent->createServiceConnection();
			dlog_print(
				DLOG_INFO,
				ACCESSORY_AGENT_LOG_TAG.c_str(),
				"Peer agent found");
		} else {
			accessoryAgent->destroyPeerAgent();
			dlog_print(
				DLOG_INFO,
				ACCESSORY_AGENT_LOG_TAG.c_str(),
				"Peer agent lost");
		}
		break;

	case SAP_PEER_AGENT_FOUND_RESULT_SERVICE_NOT_FOUND:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent result, service not found");
		break;

	case SAP_PEER_AGENT_FOUND_RESULT_TIMEDOUT:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent result, peer agent timed out");
		break;

	case SAP_PEER_AGENT_FOUND_RESULT_INTERNAL_ERROR:
		dlog_print(
			DLOG_INFO,
			ACCESSORY_AGENT_LOG_TAG.c_str(),
			"Find peer agent result, search failed");
		break;
	}
}

} // end of namespace accessory_agent
} // end of namespace services
} // end of namespace fr
