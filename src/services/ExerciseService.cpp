#include <services/ExerciseService.h>

#include <string>
#include <sstream>

#include <dlog.h>

#include "services/ServiceTools.h"

using namespace std;
using namespace rapidjson;
using namespace fr::services::accessory_agent;

namespace fr { namespace services { namespace exercise_service {

string EXERCISE_SERVICE_TAG = "ExerciseService";

ExerciseService::ExerciseService(AccessoryAgent &agent):
	_agent(agent)
{}

void ExerciseService::startExercise(int32_t workoutId) {
	ostringstream oss;
	oss << "Starting current exercise for workout id " << workoutId;
	dlog_print(DLOG_INFO, EXERCISE_SERVICE_TAG.c_str(), oss.str().c_str());
	auto message = _startExerciseRequest(workoutId);
	_agent.send(message);
}

void ExerciseService::finishExercise(int32_t workoutId, CompletedExercise &exercise) {
	ostringstream oss;
	oss << "Finishing exercise id " << exercise.exercise.id
		<< " in workout id " << workoutId;
	dlog_print(DLOG_INFO, EXERCISE_SERVICE_TAG.c_str(), oss.str().c_str());
	auto message = _finishExerciseRequest(workoutId, exercise);
	_agent.send(message);
}

std::string ExerciseService::_startExerciseRequest(int32_t workoutId) {
	Document message;
	message.SetObject();
	message.AddMember("operation", "StartExercise", message.GetAllocator());
	message.AddMember("workoutId", workoutId, message.GetAllocator());
	return documentToString(message);
}

std::string ExerciseService::_finishExerciseRequest(
	int32_t workoutId, CompletedExercise &exercise)
{
	Document completedExercise;
	completedExercise.SetObject();
	completedExercise.AddMember(
		"name",
		Value(exercise.exercise.name.c_str(), completedExercise.GetAllocator()),
		completedExercise.GetAllocator());
	completedExercise.AddMember(
		"weight",
		Value(exercise.exercise.weight.c_str(), completedExercise.GetAllocator()),
		completedExercise.GetAllocator());
	completedExercise.AddMember(
		"id", exercise.exercise.id, completedExercise.GetAllocator());
	completedExercise.AddMember(
		"previousWeight",
		Value(exercise.exercise.previousWeight.c_str(), completedExercise.GetAllocator()),
		completedExercise.GetAllocator());
	completedExercise.AddMember(
		"previousRepetitions",
		exercise.exercise.previousRepetitions,
		completedExercise.GetAllocator());
	completedExercise.AddMember(
		"weightUnit",
		Value(exercise.exercise.weightUnit.c_str(), completedExercise.GetAllocator()),
		completedExercise.GetAllocator());
	completedExercise.AddMember(
		"repetitions",
		exercise.repetitions,
		completedExercise.GetAllocator());

	Document message;
	message.SetObject();
	message.AddMember("operation", "FinishExercise", message.GetAllocator());
	message.AddMember("workoutId", workoutId, message.GetAllocator());
	message.AddMember("completedExercise", completedExercise, message.GetAllocator());
	return documentToString(message);
}

} // end of namespace exercise_service
} // end of namespace services
} // end of namespace fr
